# README #

### Description ###

This code implements Huffman Tree encoding and allows for the compression of files.
Use of Huffman encoding is provably optimal for file compression.

### How do I get set up? ###

Clone the repository and navigate to huffmancode folder.
Edit the Main.java with the files you wish to compress and the output names for encoding files.

### Future Improvements ###

I would like to make the program more flexible to allow for ease of file input and output specification

### Contribution guidelines ###

All contributions and suggestions welcome!

### Who do I talk to? ###

Hayden Clevenger
hayden.clev@gmail.com